import { HttpStatus, Injectable } from '@nestjs/common';
import { google } from 'googleapis';
import * as fs from 'fs';
import { GoogleAuth } from 'googleapis-common';

@Injectable()
export class AppService {
  private KEY_FILE = './resources/drive/credentials.json';
  private SCOPES = ['https://www.googleapis.com/auth/drive'];
  private auth: GoogleAuth;

  constructor() {
    this.auth = new google.auth.GoogleAuth({
      keyFile: this.KEY_FILE,
      scopes: this.SCOPES,
    });
  }

  async getSingleFile() {
    const driveService = google.drive({
      version: 'v3',
      auth: this.auth,
    });

    const fileMetaData = {
      name: 'zenilda.png',
      parents: ['1js0f7_2fzNziBWd--yGhN-uLF4cpLGRJ'],
    };

    const media = {
      mimeType: 'image/png',
      body: fs.createReadStream('zenilda.png'),
    };

    const response = await driveService.files.create({
      requestBody: fileMetaData,
      media: media,
      fields: 'id',
    });

    switch (response.status) {
      case 200:
        return { status: 200, message: 'Arquivo foi uploadeado com sucesso!' };
      default:
        return { status: 500, message: 'Erro inexperado!' };
    }
  }
}
