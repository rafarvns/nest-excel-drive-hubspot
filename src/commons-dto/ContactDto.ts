import { IsEmail, IsNotEmpty } from 'class-validator';

export class ContactDto {
  id: string;

  nome: string;

  sobrenome: string;

  telefone: string;

  email: string;

  datacriacao: Date;

  dataUltimaModificacao: Date;

  constructor(
    id: string,
    nome: string,
    sobrenome: string,
    email: string,
    telefone: string,
    datacriacao: Date,
    dataUltimaModificacao: Date,
  ) {
    this.id = id;
    this.nome = nome;
    this.sobrenome = sobrenome;
    this.email = email;
    this.datacriacao = datacriacao;
    this.dataUltimaModificacao = dataUltimaModificacao;
  }

  static createfromHubSpotApiContactResponse(contact: any): ContactDto {
    return new ContactDto(
      contact.id,
      contact.properties.firstname,
      contact.properties.lastname,
      contact.properties.email,
      contact.properties.phone,
      contact.properties.createdate,
      contact.properties.lastmodifieddate,
    );
  }

  static generateListFromHubspotApiContactResponse(
    contacts: any,
  ): Array<ContactDto> {
    const contactsList: Array<ContactDto> = [];
    for (const contact of contacts) {
      contactsList.unshift(this.createfromHubSpotApiContactResponse(contact));
    }
    return contactsList;
  }
}
