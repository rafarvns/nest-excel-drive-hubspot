import { IsEmail, IsNotEmpty } from 'class-validator';

export class LeadDto {
  @IsNotEmpty()
  nome: string;

  @IsNotEmpty()
  telefone: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  empresa: string;

  website: string;
}
