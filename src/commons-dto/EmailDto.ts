export class EmailDto {
  subject: string;
  emailRecipient: string;
  message: string;

  static crateWarnEmail(subject: string, emailRecipient: string): EmailDto {
    const email = new EmailDto();
    email.subject = subject;
    email.emailRecipient = emailRecipient;
    email.message = `
      <b>Your contact (${emailRecipient}) was successfully inserted or updated on HubSpot!</b>
    `;
    return email;
  }
}
