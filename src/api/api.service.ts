import { HttpStatus, Injectable } from '@nestjs/common';
import { LeadDto } from '../commons-dto/LeadDto';
import { RestResponse } from '../shared-services/common/RestResponse';
import { ExcelService } from '../shared-services/google-drive-excel/ExcelService';
import { ExcelEnum } from '../shared-services/google-drive-excel/ExcelEnum';
import { GoogleDriveService } from '../shared-services/google-drive-excel/GoogleDriveService';
import { HubSpotContatcService } from '../shared-services/hubspot/HubSpotContatcService';
import { EmailService } from '../shared-services/email/EmailService';
import { EmailDto } from '../commons-dto/EmailDto';

@Injectable()
export class ApiService {
  private gdriveService: GoogleDriveService;
  private excelService: ExcelService;

  constructor() {
    this.gdriveService = new GoogleDriveService();
    this.excelService = new ExcelService();
  }

  async insertLeads(leadDto: LeadDto): Promise<RestResponse> {
    try {
      await this.gdriveService.downloadHubSpotXlsxFileFromGoogleDrive();
      return await this.updateExcelLeadsOnDriveAndInsertOnHubSpot(leadDto);
    } catch (error) {
      return new RestResponse(
        HttpStatus.INTERNAL_SERVER_ERROR,
        true,
        error.message,
      );
    }
  }

  private async updateExcelLeadsOnDriveAndInsertOnHubSpot(
    leadDto: LeadDto,
  ): Promise<RestResponse> {
    const excelStatus =
      await this.excelService.checkInExcelFileIfContainsTheLeadAndSaysIfIsAOnlineTeam(
        leadDto,
      );

    let response;

    if (excelStatus !== ExcelEnum.HaveOnTheListAndIsNotAOnlineTeam) {
      let message = '';
      if (excelStatus === ExcelEnum.DontHaveOnTheList) {
        await this.insertExcelLeadsOnDrive(leadDto);
        await new HubSpotContatcService().insertNewLead(leadDto);
        message = 'Leads inserted!';
        response = new RestResponse(HttpStatus.CREATED, false, message);
      }

      if (excelStatus === ExcelEnum.HaveOnTheListAndIsAOnlineTeam) {
        await this.updateExcelLeadsOnDrive(leadDto);
        await new HubSpotContatcService().updateLead(leadDto);
        message = 'Leads updated!';
        response = new RestResponse(HttpStatus.OK, false, message);
      }

      const emailService = new EmailService();
      await emailService.sendMail(
        EmailDto.crateWarnEmail(message + ' on HubSpot!', leadDto.email),
      );

      return response;
    }

    return new RestResponse(
      HttpStatus.NOT_IMPLEMENTED,
      false,
      'The Lead are on excel, but does not a online team!',
    );
  }

  private async insertExcelLeadsOnDrive(leadDto: LeadDto) {
    await this.excelService.addNewLeadToExcelFile(leadDto);
    await this.gdriveService.uploadNewExcelFileToDrive();
  }

  private async updateExcelLeadsOnDrive(leadDto: LeadDto) {
    await this.excelService.updateLeadOnExcelFile(leadDto);
    await this.gdriveService.uploadNewExcelFileToDrive();
  }
}
