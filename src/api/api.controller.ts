import { Body, Controller, Post } from '@nestjs/common';
import { ApiService } from './api.service';
import { LeadDto } from '../commons-dto/LeadDto';
import { RestResponse } from '../shared-services/common/RestResponse';

@Controller('api')
export class ApiController {
  constructor(private readonly driveService: ApiService) {}

  @Post()
  async insertLeads(@Body() leadDto: LeadDto): Promise<RestResponse> {
    return await this.driveService.insertLeads(leadDto);
  }
}
