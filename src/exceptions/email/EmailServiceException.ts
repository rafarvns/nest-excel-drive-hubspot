export class EmailServiceException extends Error {
  constructor(error: any) {
    super('An error occurred while trying send the email! ' + error.message);
  }
}
