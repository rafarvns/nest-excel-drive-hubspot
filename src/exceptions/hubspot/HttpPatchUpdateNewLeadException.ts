export class HttpPatchUpdateNewLeadException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying update a lead on hubspot contacts! ' +
        error.message,
    );
  }
}
