export class HttpPostInsertNewLeadException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying insert a new lead on hubspot contacts! ' +
        error.message,
    );
  }
}
