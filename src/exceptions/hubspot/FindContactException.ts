export class FindContactException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying find a hubspot contact! ' + error.message,
    );
  }
}
