export class HttpGetLeadsException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying get the hubspot leads contacts! ' +
        error.message,
    );
  }
}
