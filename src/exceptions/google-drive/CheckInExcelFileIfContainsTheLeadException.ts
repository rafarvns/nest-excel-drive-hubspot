export class CheckInExcelFileIfContainsTheLeadException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying check if the hubspot.xlsx contains the lead! ' +
        error.message,
    );
  }
}
