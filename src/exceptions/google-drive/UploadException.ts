export class UploadException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying to upload the new version of hubspot.xlsx file! ' +
        error.message,
    );
  }
}
