export class DownloadExcelFileException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying to download hubspot.xlsx file! ' +
        error.message,
    );
  }
}
