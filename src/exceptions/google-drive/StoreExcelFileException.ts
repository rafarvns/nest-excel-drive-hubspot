export class StoreExcelFileException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying store the hubspot.xlsx file! ' +
        error.message,
    );
  }
}
