export class AddNewLeadToExcelFileException extends Error {
  constructor(error: any) {
    super(
      'An error occurred while trying update the hubspot.xlsx file! ' +
        error.message,
    );
  }
}
