import { google } from 'googleapis';
import { GoogleAuth } from 'googleapis-common';
import { UploadException } from '../../exceptions/google-drive/UploadException';
import { DownloadExcelFileException } from '../../exceptions/google-drive/DownloadExcelFileException';
import { ExcelService } from './ExcelService';

export class GoogleDriveService {
  private KEY_FILE = './resources/drive/credentials.json';
  private SCOPES = ['https://www.googleapis.com/auth/drive'];
  private FILE_ID = '1Xv4__g10e3TzGLaHBF3pTfIpInO6_mrX';
  private auth: GoogleAuth;

  constructor() {
    this.auth = new google.auth.GoogleAuth({
      keyFile: this.KEY_FILE,
      scopes: this.SCOPES,
    });
  }

  public async downloadHubSpotXlsxFileFromGoogleDrive(): Promise<void> {
    const driveService = google.drive({
      version: 'v3',
      auth: this.auth,
    });

    try {
      const response = await driveService.files.get(
        { fileId: this.FILE_ID, alt: 'media' },
        { responseType: 'stream' },
      );

      const excelService = new ExcelService();
      await excelService.storeExcelFileFromStream(response.data);
    } catch (error) {
      throw new DownloadExcelFileException(error);
    }
  }

  public async uploadNewExcelFileToDrive() {
    const driveService = google.drive({
      version: 'v3',
      auth: this.auth,
    });

    const media = {
      mimeType:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      body: new ExcelService().getExcelLocalFile(),
    };

    try {
      await driveService.files.update({
        fileId: this.FILE_ID,
        fields: 'id',
        media,
      });
    } catch (error) {
      throw new UploadException(error);
    }
  }
}
