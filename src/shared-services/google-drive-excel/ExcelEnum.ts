export enum ExcelEnum {
  HaveOnTheListAndIsAOnlineTeam,
  HaveOnTheListAndIsNotAOnlineTeam,
  DontHaveOnTheList,
}
