import * as Excel from 'exceljs';
import { Workbook, Worksheet } from 'exceljs';
import { Stream } from 'stream';
import { LeadDto } from '../../commons-dto/LeadDto';
import { ExcelEnum } from './ExcelEnum';
import { UUID } from '../common/UUID';
import * as fs from 'fs';
import { ReadStream } from 'fs';
import { AddNewLeadToExcelFileException } from '../../exceptions/google-drive/AddNewLeadToExcelFileException';
import { StoreExcelFileException } from '../../exceptions/google-drive/StoreExcelFileException';
import { CheckInExcelFileIfContainsTheLeadException } from '../../exceptions/google-drive/CheckInExcelFileIfContainsTheLeadException';

export class ExcelService {
  private readonly EXCEL_LOCAL_FILE: string;
  workbook: Workbook;

  constructor() {
    this.workbook = new Excel.Workbook();
    this.EXCEL_LOCAL_FILE = process.env.EXCEL_LOCAL_PATH;
  }

  async checkInExcelFileIfContainsTheLeadAndSaysIfIsAOnlineTeam(
    leadDto: LeadDto,
  ): Promise<ExcelEnum> {
    try {
      this.workbook = await this.workbook.xlsx.readFile(this.EXCEL_LOCAL_FILE);
      const worksheet = this.workbook.getWorksheet('hubspot');

      let count = 2;
      while (this.isAValidRow(worksheet, count)) {
        const row = worksheet.getRow(count);
        const nome = row.getCell(2).value.toString().trim();
        const equipeOnline = row.getCell(5).value;

        if (nome === leadDto.nome) {
          if (equipeOnline) {
            return ExcelEnum.HaveOnTheListAndIsAOnlineTeam;
          }
          return ExcelEnum.HaveOnTheListAndIsNotAOnlineTeam;
        }

        count++;
      }
      return ExcelEnum.DontHaveOnTheList;
    } catch (error) {
      throw new CheckInExcelFileIfContainsTheLeadException(error);
    }
  }

  async addNewLeadToExcelFile(leadDto: LeadDto): Promise<void> {
    try {
      this.workbook = await this.workbook.xlsx.readFile(this.EXCEL_LOCAL_FILE);
      const worksheet = this.workbook.getWorksheet('hubspot');
      await worksheet
        .addRow([
          UUID.generate(),
          leadDto.nome,
          leadDto.email,
          leadDto.telefone,
          true,
        ])
        .commit();
      await this.workbook.xlsx.writeFile(this.EXCEL_LOCAL_FILE);
    } catch (error) {
      throw new AddNewLeadToExcelFileException(error);
    }
  }

  async updateLeadOnExcelFile(leadDto: LeadDto) {
    try {
      this.workbook = await this.workbook.xlsx.readFile(this.EXCEL_LOCAL_FILE);
      const worksheet = this.workbook.getWorksheet('hubspot');

      let count = 2;
      while (this.isAValidRow(worksheet, count)) {
        const row = worksheet.getRow(count);
        const nome = row.getCell(2).value.toString().trim();
        if (nome === leadDto.nome) {
          row.getCell(3).value = leadDto.email;
          row.getCell(4).value = leadDto.telefone;
          row.commit();
          break;
        }
        count++;
      }
      await this.workbook.xlsx.writeFile(this.EXCEL_LOCAL_FILE);
    } catch (error) {
      throw new AddNewLeadToExcelFileException(error);
    }
  }

  storeExcelFileFromStream(excelStream: Stream): Promise<void> {
    try {
      return new Promise<void>((resolve) => {
        const writeStream = fs.createWriteStream(this.EXCEL_LOCAL_FILE);
        writeStream.on('finish', resolve);
        excelStream.pipe(writeStream);
      });
    } catch (error) {
      throw new StoreExcelFileException(error);
    }
  }

  getExcelLocalFile(): ReadStream {
    try {
      return fs.createReadStream(this.EXCEL_LOCAL_FILE);
    } catch (error) {}
  }

  private isAValidRow(worksheet: Worksheet, count: number) {
    const row = worksheet.getRow(count);
    const cell = row.getCell(1).value;
    return cell !== null && cell !== '';
  }
}
