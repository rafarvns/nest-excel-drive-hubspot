import * as nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import { Transporter } from 'nodemailer';
import { EmailDto } from '../../commons-dto/EmailDto';
import { EmailServiceException } from '../../exceptions/email/EmailServiceException';

export class EmailService {
  private transporter: Transporter<SMTPTransport.SentMessageInfo>;

  constructor() {
    this.initializeMailService();
  }

  async sendMail(mailDto: EmailDto): Promise<void> {
    if (this.transporter) {
      await this.transporter
        .sendMail({
          from: process.env.MAIL_USER,
          to: mailDto.emailRecipient,
          subject: mailDto.subject,
          text: 'teste',
          html: mailDto.message,
        })
        .catch((error) => {
          throw new EmailServiceException(error);
        });
      return;
    }
    throw new EmailServiceException('Email service not initialized correctly!');
  }

  private initializeMailService() {
    const host = process.env.SMTP_HOST;
    const port = parseInt(process.env.SMTP_PORT);
    const secure = process.env.SMTP_SECURE === 'true';
    const user = process.env.MAIL_USER;
    const pass = process.env.MAIL_PASS;

    this.transporter = nodemailer.createTransport({
      host,
      port,
      secure,
      auth: {
        user,
        pass,
      },
    });
  }
}
