import { LeadDto } from '../../commons-dto/LeadDto';
import { Http } from '../common/Http';
import { HttpGetLeadsException } from '../../exceptions/hubspot/HttpGetLeadsException';
import { HttpStatus } from '@nestjs/common';
import { ContactDto } from '../../commons-dto/ContactDto';
import { HttpPostInsertNewLeadException } from '../../exceptions/hubspot/HttpPostInsertNewLeadException';
import { FindContactException } from '../../exceptions/hubspot/FindContactException';
import { HttpPatchUpdateNewLeadException } from '../../exceptions/hubspot/HttpPatchUpdateNewLeadException';

export class HubSpotContatcService {
  private API_URL = 'https://api.hubapi.com/crm/v3/objects/contacts';
  private readonly API_KEY_QRY_PARAM: string;
  private http: Http;

  constructor() {
    this.http = new Http();
    this.API_KEY_QRY_PARAM = '?hapikey=' + process.env.API_KEY;
  }

  async getLeads(): Promise<Array<ContactDto>> {
    const url = this.API_URL + this.API_KEY_QRY_PARAM;
    return await this.http.get(url).then(
      (response) => {
        if (response.status === HttpStatus.OK) {
          return ContactDto.generateListFromHubspotApiContactResponse(
            response.data.results,
          );
        }
        throw new HttpGetLeadsException(response.statusText);
      },
      (error) => {
        throw new HttpGetLeadsException(error);
      },
    );
  }

  async getLead(contactId: string): Promise<ContactDto> {
    const url =
      this.API_URL +
      '/' +
      contactId +
      this.API_KEY_QRY_PARAM +
      '&archived=false';
    return await this.http.get(url).then(
      (response) => {
        if (response.status === HttpStatus.OK) {
          return ContactDto.createfromHubSpotApiContactResponse(response.data);
        }
        throw new HttpGetLeadsException(response.statusText);
      },
      (error) => {
        throw new HttpGetLeadsException(error);
      },
    );
  }

  async insertNewLead(leadDto: LeadDto) {
    const url = this.API_URL + this.API_KEY_QRY_PARAM;
    const website = leadDto.website ? leadDto.website : '';
    await this.http
      .post(url, {
        properties: {
          company: leadDto.empresa,
          email: leadDto.email,
          firstname: leadDto.nome,
          lastname: '',
          phone: leadDto.telefone,
          website: website,
        },
      })
      .then(
        (response) => {
          if (response.status === HttpStatus.CREATED) {
            return { status: response.status };
          }
          throw new HttpPostInsertNewLeadException(response.statusText);
        },
        (error) => {
          throw new HttpPostInsertNewLeadException(error);
        },
      );
  }

  async findContact(leadDto: LeadDto) {
    const leads = await this.getLeads();
    for (const lead of leads) {
      if (lead.nome === leadDto.nome) {
        return lead.id;
      }
    }
    return -1;
  }

  async updateLead(leadDto: LeadDto) {
    const contactId = await this.findContact(leadDto);
    if (contactId === -1) {
      throw new FindContactException('Contact Not Found 404');
    }
    const url = this.API_URL + '/' + contactId + this.API_KEY_QRY_PARAM;
    const website = leadDto.website ? leadDto.website : '';
    await this.http
      .patch(url, {
        properties: {
          company: leadDto.empresa,
          email: leadDto.email,
          firstname: leadDto.nome,
          lastname: '',
          phone: leadDto.telefone,
          website: website,
        },
      })
      .then(
        (response) => {
          if (response.status === HttpStatus.OK) {
            return { status: response.status };
          }
          throw new HttpPatchUpdateNewLeadException(response.statusText);
        },
        (error) => {
          throw new HttpPatchUpdateNewLeadException(error);
        },
      );
  }
}
