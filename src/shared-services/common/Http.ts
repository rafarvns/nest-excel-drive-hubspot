import * as axios from 'axios';
import { AxiosStatic } from 'axios';

export class Http {
  private ax: AxiosStatic;

  constructor() {
    this.ax = axios.default;
  }

  async get(url: string, params?: any) {
    return await this.ax.get(url, params).catch((error) => {
      throw new Error('Http error (GET): ' + error.message);
    });
  }

  async post(url: string, params?: any) {
    return await this.ax
      .post(url, params, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .catch((error) => {
        throw new Error('Http error (POST): ' + error.message);
      });
  }

  async patch(url: string, params?: any) {
    return await this.ax
      .patch(url, params, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .catch((error) => {
        throw new Error('Http error (PATCH): ' + error.message);
      });
  }
}
