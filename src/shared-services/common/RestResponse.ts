import { HttpStatus } from '@nestjs/common';

export class RestResponse {
  status: HttpStatus;
  isError: boolean;
  message: string;
  content: any;

  constructor(
    status: HttpStatus,
    isError: boolean,
    message: string,
    content?: any,
  ) {
    this.status = status;
    this.isError = isError;
    this.message = message;
    this.content = content;
  }
}
